package com.efabrika.mongoBasketStatus.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.efabrika.mongoBasketStatus.repository.checkout",
	mongoTemplateRef = "checkoutMongoTemplate")
public class CheckoutMongoConfig {
}
