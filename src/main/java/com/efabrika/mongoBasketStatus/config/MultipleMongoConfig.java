package com.efabrika.mongoBasketStatus.config;

import com.mongodb.MongoClient;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MultipleMongoProperties.class)
public class MultipleMongoConfig {

	private final MultipleMongoProperties mongoProperties;

	@Bean(name = "checkoutMongoTemplate")
	@Primary
	public MongoTemplate checkoutMongoTemplate() throws Exception {
		return new MongoTemplate(checkoutFactory(this.mongoProperties.getCheckout()));
	}

	@Bean(name = "basketMongoTemplate")
	public MongoTemplate basketMongoTemplate() throws Exception {
		return new MongoTemplate(basketFactory(this.mongoProperties.getBasket()));
	}

	@Bean
	@Primary
	public MongoDbFactory checkoutFactory(final MongoProperties mongoProperties) throws Exception {
		return new SimpleMongoDbFactory(
			new MongoClient(mongoProperties.getHost(), mongoProperties.getPort()), mongoProperties.getDatabase()
		);
	}

	@Bean
	public MongoDbFactory basketFactory(final MongoProperties mongoProperties) throws Exception {
		return new SimpleMongoDbFactory(
				new MongoClient(mongoProperties.getHost(), mongoProperties.getPort()), mongoProperties.getDatabase()
		);
	}

}
