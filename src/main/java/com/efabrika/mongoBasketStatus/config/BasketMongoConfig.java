package com.efabrika.mongoBasketStatus.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.efabrika.mongoBasketStatus.repository.basket",
	mongoTemplateRef = "basketMongoTemplate")
public class BasketMongoConfig {
}
