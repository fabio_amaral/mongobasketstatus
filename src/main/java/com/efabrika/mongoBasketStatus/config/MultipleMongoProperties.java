package com.efabrika.mongoBasketStatus.config;

import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@Data
@ConfigurationProperties(prefix = "mongodb")
public class MultipleMongoProperties {

	private MongoProperties checkout = new MongoProperties();
	private MongoProperties basket = new MongoProperties();

}
