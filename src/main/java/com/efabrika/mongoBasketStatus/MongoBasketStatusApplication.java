package com.efabrika.mongoBasketStatus;

import com.efabrika.mongoBasketStatus.model.StatusOnDatabases;
import com.efabrika.mongoBasketStatus.model.basket.BasketOnBasketDB;
import com.efabrika.mongoBasketStatus.model.checkout.BasketOnCheckoutDB;
import com.efabrika.mongoBasketStatus.repository.basket.BasketOnBasketDBRepository;
import com.efabrika.mongoBasketStatus.repository.checkout.BasketOnCheckoutDBRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SpringBootApplication
public class MongoBasketStatusApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoBasketStatusApplication.class, args);
	}

}

