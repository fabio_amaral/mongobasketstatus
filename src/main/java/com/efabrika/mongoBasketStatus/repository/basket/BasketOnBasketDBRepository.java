package com.efabrika.mongoBasketStatus.repository.basket;

import com.efabrika.mongoBasketStatus.model.basket.BasketOnBasketDB;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BasketOnBasketDBRepository extends MongoRepository<BasketOnBasketDB, String> {

	List<BasketOnBasketDB> findAllByBrandAndIdInAndStatusNotLike(String brand, List<String> ids, String status);

}
