package com.efabrika.mongoBasketStatus.repository.checkout;

import com.efabrika.mongoBasketStatus.model.checkout.BasketOnCheckoutDB;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BasketOnCheckoutDBRepository extends MongoRepository<BasketOnCheckoutDB, String> {

	Optional<BasketOnCheckoutDB> findById(String id);

	@Query(value = "{'brand': ?0, 'status': ?1, 'soldDate': {$gte: ?2, $lt: ?3}}")
	List<BasketOnCheckoutDB> searchBasketsOnChekout(String brand, String status, LocalDateTime beginDate, LocalDateTime endDate);

}
