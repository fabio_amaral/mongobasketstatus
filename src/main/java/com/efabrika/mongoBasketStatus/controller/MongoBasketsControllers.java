package com.efabrika.mongoBasketStatus.controller;

import com.efabrika.mongoBasketStatus.model.SearchParameter;
import com.efabrika.mongoBasketStatus.model.StatusOnDatabases;
import com.efabrika.mongoBasketStatus.service.MongoService;
import com.efabrika.mongoBasketStatus.util.APIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/{brand}")
public class MongoBasketsControllers {

	@Autowired
	MongoService mongoService;

	@PostMapping(path = "/checkStatus", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody
	APIResponse checkStatus(@RequestBody SearchParameter searchParameter) {

		List<StatusOnDatabases> statusOnDatabases = mongoService.searchStatusOnMongo(searchParameter);

		return APIResponse.buildToSuccess(null, statusOnDatabases);
	}

}
