package com.efabrika.mongoBasketStatus.service;

import com.efabrika.mongoBasketStatus.model.SearchParameter;
import com.efabrika.mongoBasketStatus.model.StatusOnDatabases;
import com.efabrika.mongoBasketStatus.model.basket.BasketOnBasketDB;
import com.efabrika.mongoBasketStatus.model.checkout.BasketOnCheckoutDB;
import com.efabrika.mongoBasketStatus.repository.basket.BasketOnBasketDBRepository;
import com.efabrika.mongoBasketStatus.repository.checkout.BasketOnCheckoutDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MongoService {

	@Autowired
	private BasketOnCheckoutDBRepository checkoutDBRepository;

	@Autowired
	private BasketOnBasketDBRepository basketDBRepository;

	public List<StatusOnDatabases> searchStatusOnMongo(SearchParameter params) {
		List<StatusOnDatabases> statusOnDatabases = new ArrayList<>();

		//Buscando as baskets no banco do checkout
		List<BasketOnCheckoutDB> basketsOnCheckoutDB = checkoutDBRepository.searchBasketsOnChekout(
				params.getBrand(), params.getStatus(), params.getBeginDate(), params.getEndDate());

		//Separando os ids para buscar as basket no banco da basket
		List<String> basketsIdsOnCheckoutDB = basketsOnCheckoutDB.stream()
				.map(basket -> basket.getId())
				.collect(Collectors.toCollection(ArrayList::new));

		//Buscando as baskets no banco da basket
		List<BasketOnBasketDB> basketsOnBasketDB = basketDBRepository.findAllByBrandAndIdInAndStatusNotLike(params.getBrand(), basketsIdsOnCheckoutDB, "SOLD_SUCCESS");

		//Criando a lista com os status de ambos os bancos
		basketsOnBasketDB.forEach(basketOnBasketDB ->
						statusOnDatabases.add(StatusOnDatabases.builder()
								.basketId(basketOnBasketDB.getId())
								.soldDate(basketOnBasketDB.getSoldDate())
								.checkout(basketsOnCheckoutDB.stream().filter(b -> b.getId().equals(basketOnBasketDB.getId())).map(b -> b.getStatus()).findFirst().orElse(null))
								.basket(basketOnBasketDB.getStatus())
								.build())

				);

		return statusOnDatabases;
	}

}
