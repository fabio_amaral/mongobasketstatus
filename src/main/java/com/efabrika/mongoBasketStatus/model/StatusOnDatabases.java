package com.efabrika.mongoBasketStatus.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class StatusOnDatabases {

	String basketId;
	String checkout;
	String basket;
	LocalDateTime soldDate;
}
