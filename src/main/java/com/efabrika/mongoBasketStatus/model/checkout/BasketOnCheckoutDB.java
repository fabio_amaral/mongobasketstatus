package com.efabrika.mongoBasketStatus.model.checkout;

import com.efabrika.mongoBasketStatus.model.BasketCreationType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

@Data
@Builder
@Document(collection = "basket")
@JsonIgnoreProperties(ignoreUnknown = true)
public class BasketOnCheckoutDB {

	@Id
	private String id;

	@NotNull
	private String brand;

	private String status = STATUS_PENDING;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private LocalDateTime soldDate;

	/* Constants */
	public static final String STATUS_CANCELED = "canceled";
	public static final String STATUS_DISCARDED = "discarded";
	public static final String STATUS_TO_LATER = "toLater";
	public static final String STATUS_SOLD = "sold";
	public static final String STATUS_SOLD_SUCCESS = "sold_success";
	public static final String STATUS_SOLDING = "solding";
	public static final String STATUS_SOLDING_ERROR = "solding_error";
	public static final String STATUS_PENDING = "pending";
	public static final String STATUS_SHOPPING_LIST = "shopping_list";
	public static final String STATUS_LAST_ORDER = "last_order";
	public static final String STATUS_CHANGE_PAYMENT = "change_payment";
	public static final String STATUS_PENDING_SUBSCRIPTION = "pending_subscription";
	public static final List<String> basketStatus = Arrays.asList(BasketOnCheckoutDB.STATUS_SOLD,
			BasketOnCheckoutDB.STATUS_SOLD_SUCCESS,
			BasketOnCheckoutDB.STATUS_CANCELED,
			BasketOnCheckoutDB.STATUS_SHOPPING_LIST,
			BasketOnCheckoutDB.STATUS_LAST_ORDER);

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private LocalDateTime createdDate;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private LocalDateTime lastmodifiedDate;


}
