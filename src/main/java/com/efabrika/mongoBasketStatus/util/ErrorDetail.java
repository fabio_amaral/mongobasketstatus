package com.efabrika.mongoBasketStatus.util;

public class ErrorDetail {
	public static ErrorDetail buildToPaymentForm(String code, String message, Long sequence) {
		ErrorDetail ed = new ErrorDetail();
		ed.code = code;
		ed.message = message;
		ed.entity = "paymentForm";
		ed.entitySequence = sequence;

		return ed;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Long getEntitySequence() {
		return entitySequence;
	}

	public void setEntitySequence(Long entitySequence) {
		this.entitySequence = entitySequence;
	}

	private String code;
	private String message;
	private String entity;
	private Long entitySequence;
}
